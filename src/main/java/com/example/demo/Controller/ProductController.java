package com.example.demo.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller(value = "/product")
public class ProductController {

    @RequestMapping(value = "/add")
    public String addProductPage(){
        return "add";
    }

    @RequestMapping(value = "/list")
    public String productListPage(){
        return "productList";
    }
}
