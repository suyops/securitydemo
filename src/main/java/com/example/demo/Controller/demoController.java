package com.example.demo.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Controller
public class demoController {
@RequestMapping(value = "/index")
    public String indexPage(){
        return "index";

    }
    @RequestMapping(value = "/403")
    public String errorPage(){
    return "errorPage";
    }

    @RequestMapping(value = "/login1")
    public String loginPage(){
        return "login1";

    }
}
